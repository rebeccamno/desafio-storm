# coding=utf-8

from django.contrib import admin

from .models import Actor, Genre, Movie


class GenreAdmin(admin.ModelAdmin):

    list_display = ['name', 'slug']
    search_fields = ['name', 'slug']
    prepopulated_fields = {'slug': ('name',)}


class ActorAdmin(admin.ModelAdmin):

    list_display = ['name', 'country']
    search_fields = ['name', ]


class MovieAdmin(admin.ModelAdmin):

    list_display = ['name', 'summary']
    search_fields = ['name', ]
    list_filter = ['genres__name']
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(Genre, GenreAdmin)
admin.site.register(Actor, ActorAdmin)
admin.site.register(Movie, MovieAdmin)
