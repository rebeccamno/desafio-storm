# coding=utf-8

from rest_framework.generics import ListAPIView, RetrieveAPIView

from movies.models import Actor, Movie

from .serializers import (ActorSerializer, MovieDetailSerializer,
                          MovieListSerializer)


class ActorListAPIView(ListAPIView):
    serializer_class = ActorSerializer

    def get_queryset(self, *args, **kwargs):
        queryset_list = Actor.objects.all()
        return queryset_list


class MovieListAPIView(ListAPIView):

    serializer_class = MovieListSerializer

    def get_queryset(self, *args, **kwargs):
        queryset_list = Movie.objects.all()
        genre = self.request.query_params.get('genre', None)
        if genre is not None:
            queryset_list = queryset_list.filter(genres__slug=genre)
        return queryset_list


class MovieDetailAPIView(RetrieveAPIView):
    queryset = Movie.objects.all()
    serializer_class = MovieDetailSerializer
    lookup_field = 'slug'
