# coding=utf-8
from rest_framework import serializers

from movies.models import Actor, Genre, Movie


class GenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = Genre
        fields = ('name', 'slug')


class ActorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Actor
        fields = ('name', 'country', 'image')


class MovieListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Movie
        fields = ('name', 'slug', 'summary', 'image')


class RelatedMovieSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)
    slug = serializers.SlugField(max_length=100)
    image = serializers.ImageField()
    num_actors = serializers.IntegerField()
    num_genres = serializers.IntegerField()
    total = serializers.IntegerField()


class MovieDetailSerializer(serializers.ModelSerializer):
    actors = ActorSerializer(many=True, read_only=True)
    genres = GenreSerializer(many=True, read_only=True)
    related_movies = serializers.SerializerMethodField()

    def get_related_movies(self, obj):
        related_movies_qs = obj.related_movies
        related_movies = RelatedMovieSerializer(
            related_movies_qs, many=True).data

        return related_movies

    class Meta:
        model = Movie
        fields = ('name', 'summary', 'image',
                  'genres', 'actors', 'related_movies')
