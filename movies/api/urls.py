# coding=utf-8

from django.conf.urls import url

from .views import MovieDetailAPIView, MovieListAPIView

urlpatterns = [
    url(r'^$', MovieListAPIView.as_view(), name='list'),
    url(r'^(?P<slug>[\w-]+)/$', MovieDetailAPIView.as_view(), name='detail'),
]
