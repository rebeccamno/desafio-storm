# coding=utf-8

import os
import uuid

from django.db import models
from django.db.models import Count, Q
from django.utils import timezone
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Genre(models.Model):
    name = models.CharField('Gênero', max_length=100)
    slug = models.SlugField('Slug', max_length=100, unique=True)

    class Meta:
        verbose_name = (u'Gênero')
        verbose_name_plural = (u'Gêneros')
        ordering = ['name']

    def __str__(self):
        return self.name


def path_and_rename_image(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    today = timezone.now()
    path = '{0}_{1}/{2}/{3}'.format(
        instance._meta.app_label, instance.__class__.__name__,
        today.year, today.month)
    return os.path.join(path.lower(), filename)


@python_2_unicode_compatible
class Actor(models.Model):
    name = models.CharField('Nome', max_length=100)
    country = models.ForeignKey('core.Country', verbose_name="País")
    image = models.ImageField(upload_to=path_and_rename_image)

    class Meta:
        verbose_name = 'Ator'
        verbose_name_plural = 'Atores'
        ordering = ['name']

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Movie(models.Model):
    name = models.CharField('Nome', max_length=100)
    slug = models.SlugField('Slug', max_length=100, unique=True)
    summary = models.TextField('Sinopse')
    image = models.ImageField(upload_to=path_and_rename_image)
    genres = models.ManyToManyField('movies.Genre', verbose_name="Gêneros")
    actors = models.ManyToManyField('movies.Actor', verbose_name="Atores")

    class Meta:
        verbose_name = 'Filme'
        verbose_name_plural = 'filmes'
        ordering = ['name']

    def __str__(self):
        return self.name

    def related_actors(self):
        related_actors = Movie.objects.filter(
            actors__in=self.actors.values_list('id', flat=True)
        ).annotate(
            num_actors=Count('actors', distinct=True)
        ).exclude(id=self.id).values('id', 'num_actors')

        return related_actors

    def related_genres(self):
        related_genres = Movie.objects.filter(
            genres__in=self.genres.values_list('id', flat=True)
        ).annotate(
            num_genres=Count('genres', distinct=True)
        ).exclude(id=self.id).values('id', 'num_genres')

        return related_genres

    @property
    def related_movies(self):

        related_movies_qs = Movie.objects.filter(
            Q(actors__in=self.actors.all()) | Q(genres__in=self.genres.all())
        ).values('id', 'name', 'image', 'slug').exclude(id=self.id).distinct()

        in_commom = [(x['id'], {'total': 0, 'num_actors': 0,
                                'num_genres': 0}) for x in related_movies_qs]
        in_commom = dict(in_commom)

        actors_qs = self.related_actors()
        genres_qs = self.related_genres()

        [in_commom.get(x['id']).update(
            {'num_actors': x['num_actors']}) for x in actors_qs]

        [in_commom.get(x['id']).update(
            {'num_genres': x['num_genres']}) for x in genres_qs]

        [in_commom.get(x['id']).update(
            {'total': in_commom.get(x['id'])['num_actors'] + in_commom.get(x['id'])['num_genres']}) for x in genres_qs]

        [x.update(in_commom.get(x['id'])) for x in related_movies_qs]

        return related_movies_qs
