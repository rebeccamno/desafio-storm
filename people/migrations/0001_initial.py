# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2017-11-24 20:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0007_alter_validators_add_error_messages'),
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('name', models.CharField(max_length=100, verbose_name=b'Nome')),
                ('email', models.EmailField(max_length=254, unique=True, verbose_name=b'E-mail')),
                ('is_staff', models.BooleanField(default=False, verbose_name=b'Equipe')),
                ('is_active', models.BooleanField(default=True, verbose_name=b'Ativo')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name=b'Cadastrado em')),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'Cadastro',
                'verbose_name_plural': 'Cadastros',
            },
        ),
    ]
