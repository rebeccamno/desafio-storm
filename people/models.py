# coding=utf-8

from django.contrib.auth.models import (AbstractBaseUser, BaseUserManager,
                                        PermissionsMixin)
from django.db import models
from django.utils.encoding import python_2_unicode_compatible


class PersonManager(BaseUserManager):

    def verify_if_has_email_and_password(self, email, password):
        if not email and not password:
            raise ValueError('Users must have an email and password')

    def create_superuser(self, email, password):
        self.verify_if_has_email_and_password(email, password)
        user = self.model(email=email)
        user.is_active = True
        user.is_staff = True
        user.is_superuser = True
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_user(self, email, name, password):
        name = name
        email = self.normalize_email(email)
        user = self.model(email=email, name=name)
        user.is_active = True
        user.set_password(password)
        user.save(using=self._db)
        return user


@python_2_unicode_compatible
class Person(AbstractBaseUser, PermissionsMixin):
    name = models.CharField('Nome', max_length=100)
    email = models.EmailField('E-mail', unique=True)
    is_staff = models.BooleanField('Equipe', default=False)
    is_active = models.BooleanField('Ativo', default=True)
    created = models.DateTimeField('Cadastrado em', auto_now_add=True)

    USERNAME_FIELD = 'email'

    objects = PersonManager()

    class Meta:
        verbose_name = 'Cadastro'
        verbose_name_plural = 'Cadastros'

    def __str__(self):
        return self.name

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name
